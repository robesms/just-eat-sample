﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustEat.Core.Model
{
    public class CuisineType
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string SeoName { get; set; }
    }
}
