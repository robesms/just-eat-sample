﻿export interface Restaurant {
    Name: string;
    RatingForDisplay: number;
}
