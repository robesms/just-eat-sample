﻿using System;
using System.Collections.Generic;
using JustEat.Core.Model;
using JustEat.Core.Helper;

namespace JustEat.Client.Components
{
    public class RestaurantClient : IRestaurantClient
    {
        private IRequester _requester;
       

        public RestaurantClient(IRequester requester)
        {
            _requester = requester;
        }

        public string ResourceUrl
        {
            get { return "/restaurants"; }
        }

        public IList<GetRestaurantResult> Search(string outcode)
        {
            return _requester.Get<RestaurantSearchResult>(ResourceUrl + "/?q=" + outcode).Restaurants;
        }
    }
}
