﻿using JustEat.Core.Helper;
using JustEat.Client.Components;

namespace JustEat.Client
{
    public class JustEatClient : IJustEatClient
    {
        private readonly IRequester _requester;
        public IRestaurantClient Restaurants { get; }

        public JustEatClient(string host, string secret)
        {
            _requester = new Requester(host, secret);
            Restaurants = new RestaurantClient(_requester);

        }
    }
}
