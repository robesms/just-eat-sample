﻿using JustEat.Client;
using JustEat.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    public class SearchController : ApiController
    {
        private IJustEatClient client = new JustEatClient("http://public.je-apis.com","VGVjaFRlc3RBUEk6dXNlcjI=");

        [HttpGet]
        public IEnumerable<GetRestaurantResult> Get(string outcode)
        {
            return client.Restaurants.Search(outcode);
        }
    }
}
