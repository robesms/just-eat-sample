﻿Feature: Restaurants
	As a user running the application
	I can view a list of restaurants in a user submitted outcode (ex. SE19) **
	So that I know which restaurants are currently available**

@mytag
Scenario: When I search for outcode, results are returned
	Given The outcode 'SE19'
	When I search for restaurants by outcode
	Then I receive a list of restaurans