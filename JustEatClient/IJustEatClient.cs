﻿using JustEat.Core.Model;
using JustEat.Client.Components;
using System.Collections.Generic;

namespace JustEat.Client
{
    public interface IJustEatClient
    {
        IRestaurantClient Restaurants { get; } 
    }
}