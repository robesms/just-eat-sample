﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using JustEat.Core.Helper;
using JustEat.Client.Components;
using JustEat.Core.Model;

namespace JustEat.Client.Test
{
    [TestClass]
    public class RestauratClientTest
    {
        [TestMethod]
        public void Search_Call_UsesSearchUrl()
        {
            Mock<IRequester> mockRequester = new Mock<IRequester>();
            mockRequester.Setup(r => r
                .Get<RestaurantSearchResult>(It.IsAny<string>()))
                .Returns(new RestaurantSearchResult());

            var restaurantClient = new RestaurantClient(mockRequester.Object);

            restaurantClient.Search("se19");

            mockRequester.Verify(m => m.Get<GetRestaurantResult>("/restaurants/?q=se19"));
        }
    }
}
