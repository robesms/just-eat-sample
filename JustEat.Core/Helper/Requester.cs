﻿using JustEat.Core.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace JustEat.Core.Helper
{
    public class Requester : IRequester
    {
        private string _host;
        private string _secret;

        public Requester(string host, string secret)
        {
            _host = host;
            _secret = secret;
        }


        public T Get<T>(string tailUrl)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_host);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", _secret);
                client.DefaultRequestHeaders.Add("Accept-Tenant", "uk");
                client.DefaultRequestHeaders.Add("Accept-Language", "en-GB");
                client.DefaultRequestHeaders.Add("Accept-Version", "v1");
                var response = client.GetAsync(tailUrl).Result;
                var json = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<T>(json);

            }

        }
    }
}
