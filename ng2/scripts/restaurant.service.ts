﻿import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';
import {Restaurant} from './restaurant';


@Injectable()
export class RestaurantService {

    constructor(private http: Http) { }
    private _url = 'http://localhost:54490/api/search/';
 

    get(outcode: string) {
        return this.http.get(this._url + outcode)
            .map(res => <Restaurant[]>res.json())
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}