﻿import {Component} from 'angular2/core';
import {Restaurant} from './restaurant';

@Component({
    selector: 'my-restaurant-detail',
    template: `<div *ngIf="restaurant">
               <h2>{{restaurant.Name}} details!</h2>
               <div>
                  details here ...
               </div>
             </div>`,
    inputs: ['restaurant']
})

export class RestaurantDetailComponent {
    public restaurant: Restaurant;
}
