﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustEat.Core.Model
{
    public class GetRestaurantResult
    {
        public string Name { get; set; }
        public double RatingForDisplay { get; set; }
        public IList<CuisineType> CuisineTypes { get; set;}
    }
}
