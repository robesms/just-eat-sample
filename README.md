# Technical Solution

Simple implementation of http client, served by web api to angular 2 app.

Used the oportunity to try out the new .NET Core project template, angular 2 and typescript.

Didn't use a TDD/BDD approach for this sample, a bit rusty, the 2 basic tests were added prior to development. Given more time would have liked to try a test first approach.

Also, given the short time, a couple of very important things are missing. (exception handling, dependency management, configuration, better project structure for the single page app ...)


## Structure
- http client
- core class library
- web api
- angular 2

### Tests
- specflow + nunit
- ms test + moq

### Requirements
-  JustEatClient & WebApi
	-  	.NET 4.5

-  Web application (Angular 2)
	-  	.NET Core
	-   npm


# Questions

## Time spent

Test took a couple of shorter sessions, probably above 4h because I wanted to  try out a few new things which I haven't used before.


## Improvements

### Backend

- Exception handling/Logging
- Better abstractions: single responsibility
- Unit tests: better abstractions would allow for better test support
- Caching mechanism:searching for same outcode during short intervals
- Throttling mechanism: limit number of requests per second/minute/hour
- Dependency injection: *SimpleInjector*
- Client configuration: especially sensitive data, this sould not be stored in same repository as app, a different repository could be used for tranformations and a tool like *Octopus Deploy* can manage the tranform



### Frontend optimisation (build scripts)

 - npm scripts can be used instead of gulp for concatenation, min, uglifying scripts, simpler and do not require knowledge of vendor specific tooling
 - caching resources (images ... etc)
 - tests (jasmine and phantomjs)



# Latest usefull feature

I'm currently using a Mac and Parallels for developing .NET so the new open source strategy  with .NET Core (CoreFX) and .NET CLR is very interesting.

VS2015 also comes with good features. NPM integration, Architecture explorer which was only available in Ultimate Edition.


# Performance issues in production

I've used the following tools in the past.

- ELK stack (AWS or self hosted) for real time analytics and visualisation
- Azure App Insights (free tier)
- Dynatrace - pure paths provide drill down information from server to DB level
- New Relic for infrastructure monitoring



# Possible API improvement

## HATEOAS

Something which I think would be useful is using HATEOAS to provide follow-up links via the response
`
{
  "_links" : [
    {
      "rel" : "book"
      "href" : "book/id"
    }
  ]
}
`


## Field selection

`GET /restaurants?fields=Price`



# JSON description

```json
{
  "name": "Alexandru Ionica",

    "info": {
      "label": "Developer",
      "brief": " Passionate, meticulous, goal-oriented technology enthusiast. From hands-on development and delivery with a broad mix of languages and technologies to enabling DevOps and engineering practices, to leading development teams, I have built a career providing solid solutions in IT."
    },

    "contact": {
      "website": "http://thedev.ro",
      "phone": "",
      "other": [ ]
    },

    "location": {
      "city": "London",
      "country": "Uk"
    },
    "social": [
    {
      "label": "GitHub",
      "network": "GitHub",
      "user": "thedev",
      "url": "https://github.com/thedev"
    },

    {
      "label": "Twitter",
      "network": "Twitter",
      "user": "the_dev",
      "url": "https://twitter.com/the_dev"
    }
  ]
}
```


