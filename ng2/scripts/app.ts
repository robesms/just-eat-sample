﻿import {Component} from 'angular2/core';
import {OnInit} from 'angular2/core';
import {HTTP_PROVIDERS}    from 'angular2/http';
import 'rxjs/Rx';
import {Restaurant} from './restaurant';
import {RestaurantDetailComponent} from './restaurant-detail.component';
import {RestaurantService} from './restaurant.service';



@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    styleUrls: ['app/app.component.css'],
    directives: [RestaurantDetailComponent],
    providers: [HTTP_PROVIDERS, RestaurantService]
})

export class AppComponent implements OnInit {
    public title = 'Search Restaurants';
    errorMessage: string;
    public restaurants: Restaurant[];
    public selectedRestaurant: Restaurant;

    constructor(private _restaurantService: RestaurantService) { }

    ngOnInit() {
    }

    onSelect(restaurant: Restaurant) {
        this.selectedRestaurant = restaurant;
    }

    search(outcode: string) {
        this._restaurantService.get(outcode).subscribe(restaurants => this.restaurants = restaurants,
            error => this.errorMessage = <any>error);
    }
}


