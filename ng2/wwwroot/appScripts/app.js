var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('angular2/core');
var http_1 = require('angular2/http');
require('rxjs/Rx');
var restaurant_detail_component_1 = require('./restaurant-detail.component');
var restaurant_service_1 = require('./restaurant.service');
var AppComponent = (function () {
    function AppComponent(_restaurantService) {
        this._restaurantService = _restaurantService;
        this.title = 'Search Restaurants';
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent.prototype.onSelect = function (restaurant) {
        this.selectedRestaurant = restaurant;
    };
    AppComponent.prototype.search = function (outcode) {
        var _this = this;
        this._restaurantService.get(outcode).subscribe(function (restaurants) { return _this.restaurants = restaurants; }, function (error) { return _this.errorMessage = error; });
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: 'app/app.component.html',
            styleUrls: ['app/app.component.css'],
            directives: [restaurant_detail_component_1.RestaurantDetailComponent],
            providers: [http_1.HTTP_PROVIDERS, restaurant_service_1.RestaurantService]
        }), 
        __metadata('design:paramtypes', [restaurant_service_1.RestaurantService])
    ], AppComponent);
    return AppComponent;
})();
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.js.map