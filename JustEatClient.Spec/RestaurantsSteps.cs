﻿using JustEat.Client;
using JustEat.Core.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace JustEat.Client.Spec
{
    [Binding]
    public class RestaurantsSteps
    {
        private string _outcode;
        private IJustEatClient _client;
        private IList<GetRestaurantResult> _result;

        [BeforeScenario]
        public void BeforeScenario()
        {
            _client = new JustEatClient("http://public.je-apis.com","VGVjaFRlc3RBUEk6dXNlcjI=");
        }

        [Given(@"The outcode '(.*)'")]
        public void GivenTheOutcode(string outcode)
        {
            _outcode = outcode; 
        }
        
        [When(@"I search for restaurants by outcode")]
        public void WhenISearchForRestaurantsByOutcode()
        {
            _result = _client.Restaurants.Search(_outcode);
        }
        
        [Then(@"I receive a list of restaurans")]
        public void ThenIReceiveAListOfRestaurans()
        {
            Assert.IsNotNull(_result);
            Assert.True(_result.Count > 0);
        }
    }
}
