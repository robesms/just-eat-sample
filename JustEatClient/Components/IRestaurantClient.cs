﻿using JustEat.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustEat.Client.Components
{
    public interface IRestaurantClient
    {
        string ResourceUrl { get; }
        IList<GetRestaurantResult> Search(string outcode);
    }
}
