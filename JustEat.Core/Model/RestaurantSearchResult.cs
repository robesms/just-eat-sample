﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustEat.Core.Model
{
    public class RestaurantSearchResult
    {
        public string ShortResultText { get; set; }
        public IList<GetRestaurantResult> Restaurants { get; set; }
    }
}
